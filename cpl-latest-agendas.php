<?php
/**
 * Plugin Name:     CPL Latest Agendas Block
 * Description:     Dynamic Block that loads a list of all published CPL Board Agendas; uses create-block
 * Version:         0.1.2
 * Author:          Will Skora and the CPL Team
 * License:         GPL-2.0-or-later
 * License URI:     https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:     cpl-latest-agendas
 *
 * @package         create-block
 */

/**
 * Registers all block assets so that they can be enqueued through the block editor
 * in the corresponding context.
 *
 * @see https://developer.wordpress.org/block-editor/tutorials/block-tutorial/applying-styles-with-stylesheets/
 */


 namespace CPL\cpl_blocks\cpl_latest_agendas;

function create_block_cpl_latest_agendas_block_init() {
	$dir = __DIR__;

	$script_asset_path = "$dir/build/index.asset.php";
	if ( ! file_exists( $script_asset_path ) ) {
		throw new Error(
			'You need to run `npm start` or `npm run build` for the "create-block/cpl-latest-agendas" block first.'
		);
	}
	$index_js     = 'build/index.js';
	$script_asset = require( $script_asset_path );
	wp_register_script(
		'create-block-cpl-latest-agendas-block-editor',
		plugins_url( $index_js, __FILE__ ),
		$script_asset['dependencies'],
		$script_asset['version']
	);
	wp_set_script_translations( 'create-block-cpl-latest-agendas-block-editor', 'cpl-latest-agendas' );

	$editor_css = 'build/index.css';
	wp_register_style(
		'create-block-cpl-latest-agendas-block-editor',
		plugins_url( $editor_css, __FILE__ ),
		array(),
		filemtime( "$dir/$editor_css" )
	);

	$style_css = 'build/style-index.css';
	wp_register_style(
		'create-block-cpl-latest-agendas-block',
		plugins_url( $style_css, __FILE__ ),
		array(),
		filemtime( "$dir/$style_css" )
	);

	register_block_type(
		'create-block/cpl-latest-agendas',
		array(
			'editor_script'   => 'create-block-cpl-latest-agendas-block-editor',
			'editor_style'    => 'create-block-cpl-latest-agendas-block-editor',
			'style'           => 'create-block-cpl-latest-agendas-block',
			'render_callback' => __NAMESPACE__ . '\cpl_agendas_callback',
		)
	);
}
add_action( 'init', __NAMESPACE__ . '\create_block_cpl_latest_agendas_block_init' );


function cpl_agendas_callback( $block_attributes, $content ) {
	// RECS FROM 10UP https://10up.github.io/Engineering-Best-Practices/php/#namespacing
	$args = array(
		'ignore_sticky_posts'    => 1,
		'nopaging'               => true,
		'no_found_rows'          => true,
		'post_status'            => array( 'publish' ),
		'post_type'              => array( 'cpl_agenda' ),
		'posts_per_page'         => 500,
		'update_post_term_cache' => false,
	);

	// The Query
	$agenda_query = new \WP_Query( $args );

	if ( $agenda_query->have_posts() ) {
		$teh_html .= '<ul>';
	}
	while ( $agenda_query->have_posts() ) {
		$agenda_query->the_post();
		$post_id   = get_the_ID();
		$teh_html .= sprintf(
			'<li><a href="%1$s">%2$s</a>',
			esc_url( get_permalink( $post_id ) ),
			esc_html( get_the_title( $post_id ) ),
			'</li>'
		);
	}
	$teh_html .= '</ul>';
		return $teh_html;

}
